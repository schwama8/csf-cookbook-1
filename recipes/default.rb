case node["deploy_env"]
when nil, ''
  csfenv=''
when 'release'
  csfenv='prod'
when 'ci'
  csfenv='dev'
when 'NEB-2030'
  csfenv='dev'
else
  csfenv=deploy_env
end

node.default['tomcat']['base_version'] = 7
include_recipe "mw-tomcat"


nibr_ssl "csf ssl for tomcat7" do
     root_cert_source 'pki_trust'
     project 'csf'
     cid_env node["deploy_env"]
 end

package 'java-1.7.0-openjdk-debuginfo'

remote_file "#{node[:tomcat][:config_dir]}/csf.properties" do
  source "http://svn.nibr.novartis.intra/svn/NITAS_Sigma_CHBS/CSF/trunk/config/#{csfenv}/csf.properties"
  user node[:tomcat][:user]
  group node[:tomcat][:group]  
  mode 0660
end

template "#{node[:tomcat][:config_dir]}/csf.passwords" do
  owner "#{node['tomcat']['user']}"
  group "#{node['tomcat']['group']}"
  mode 0600
  source "csf.passwords.#{csfenv}.erb"
  variables(
    :dbpassword => data_bag_item("projects", "csf")["password_#{csfenv}"],
    :workbenchappapikey => data_bag_item("projects", "csf")["workbench_#{csfenv}"]
  )
  action :create
end

deploy_war "ROOT.war" do
  #project "csf-dev-build"
  #source "archive"
  #version node["csf"]["version"] if !node["csf"].nil?

  project node["project"]
  source node["project_info"]["pinpromote"]["source"]
  version node["project_info"]["pinpromote"]["version"]

  jenkins_artifact "csf-frontend-SNAPSHOT.war"
end

deploy_war "csf.war" do
  #project "csf-dev-build"
  #source "archive"
  #version node["csf"]["version"] if !node["csf"].nil?

  project node["project"]
  source node["project_info"]["pinpromote"]["source"]
  version node["project_info"]["pinpromote"]["version"]

  jenkins_artifact "csf-backend-SNAPSHOT.war"
end


# to make it possible for Manuel to edit sysconfig 
# it should be under tomcat ownership
file "/etc/sysconfig/tomcat#{node[:tomcat][:base_version]}" do
  owner 'tomcat'
  group 'tomcat'
  mode '0644'
end

