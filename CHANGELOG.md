# 2.0.1
- by melezhik - enable_welcome_file_list upon Schwarze, Manuel request 

# 2.0.0
- by melezhik - merge NEB-4120 to trunk 

# 0.0.2
- [NEB-2869] - by melezhik - custom JAVA_OPTS

# 0.0.1 
- [NEB-2410] use mw-tomcat
- installs java-1.7.0-openjdk-debuginfo.x86_64 package by Manuel request
- tomcat sysconfig is under tomcat ownership


